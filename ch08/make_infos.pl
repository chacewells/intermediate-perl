#!/usr/bin/perl
use v5.18;
use IO::File;

while (<>) {
    state %out_files;
    my ($character) = /^(.*):/;
    $character =~ s/[\s\.]+/_/g;
    my $character_file = "\L${character}.info";
    unless (defined $out_files{$character_file}) {
        $out_files{$character_file} = IO::File->new($character_file, 'w');
    }
    $out_files{$character_file}->print($_);
}
