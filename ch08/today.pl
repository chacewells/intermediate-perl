#!/usr/bin/perl
use v5.18;
use Getopt::Std qw# getopts #;
use IO::File;
use IO::Tee;
use IO::Scalar;

my (%opts, @outs, $str);
getopts('sf:', \%opts);

push @outs, IO::File->new($opts{f}, 'w') if $opts{f};
push @outs, IO::Scalar->new(\$str) if $opts{s};

{
    my $tee = IO::Tee->new(@outs);
    my ($day, $mon, $year, $wday) = (localtime)[3..6];
    my @wdays = qw( Sun Mon Tue Wed Thu Fri Sat );
    $tee->printf("%s %4d-%02d-%02d\n", $wdays[$wday], $year+1900, $mon+1, $day);
}

print "here's your string: $str" if defined $str;
