#!/usr/bin/perl
use v5.18;
use IO::Dir;
use IO::Tee;
use IO::File;

sub print_contents {
     my ($dir, $dh, $fh) = @_;
     print $fh "$dir:\n";
     $fh->print(' 'x4, "$_\n") while defined( $_ = $dh->read );
}

my $tee = new IO::Tee((new IO::File 'dirlisting.txt', 'w'), \*STDOUT);
print_contents($_, IO::Dir->new($_), $tee) foreach @ARGV;
