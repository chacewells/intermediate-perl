#!/usr/bin/perl
use List::Util qw( sum );
use JSON qw( to_json from_json );
use constant { RUNNING_TOTALS_FILE => 'running_totals.json' };

my %byte_data;

eval {
    open my $running_totals_file, '<', RUNNING_TOTALS_FILE;
    my $running_totals_data = from_json <$running_totals_file>;
    %byte_data = %{ $running_totals_data };
    close $running_totals_file;
    1
} or warn "$!";

foreach (<>) {
    my($source, $dest, $bytes) = split;
    $byte_data{$source}{$dest} += $bytes;
}

eval {
    my $running_totals_json = to_json \%byte_data;
    open my $running_totals_file, '>', RUNNING_TOTALS_FILE or die "that didn't work: $!";
    print $running_totals_file $running_totals_json;
    close $running_totals_file;
} or die "$!";

open my $out, '>', 'coconet_out.dat';

foreach $source (sort { $a cmp $b } keys %byte_data) {
    print $out "$source\n";
    print $out "  $_ $byte_data{$source}{$_}\n" foreach sort { $a cmp $b } keys %{ $byte_data{$source} };
}
close $out;
