#!/usr/bin/perl
use List::Util qw( sum );
use YAML qw( LoadFile DumpFile );
use constant { RUNNING_TOTALS_FILE => 'running_totals.yaml' };

my %byte_data;

eval {
    my $running_totals_data = LoadFile RUNNING_TOTALS_FILE;
    %byte_data = %{ $running_totals_data };
    1
} or warn "$!";

foreach (<>) {
    my($source, $dest, $bytes) = split;
    $byte_data{$source}{$dest} += $bytes;
}

eval {
    DumpFile RUNNING_TOTALS_FILE, \%byte_data or die "that didn't work: $!";
} or die "$!";

open my $out, '>', 'coconet_out.dat';

foreach $source (sort { $a cmp $b } keys %byte_data) {
    print $out "$source\n";
    print $out "  $_ $byte_data{$source}{$_}\n" foreach sort { $a cmp $b } keys %{ $byte_data{$source} };
}
close $out;
