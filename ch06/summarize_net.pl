#!/usr/bin/perl
use List::Util qw( sum );
use Storable qw( thaw retrieve freeze nstore_fd );
use constant { RUNNING_TOTALS_FILE => 'running_totals.dat' };

my %byte_data;

eval {
    my $running_totals_data = retrieve RUNNING_TOTALS_FILE;
    %byte_data = %{ $running_totals_data };
    1
} or warn "$!";

foreach (<>) {
    my($source, $dest, $bytes) = split;
    $byte_data{$source}{$dest} += $bytes;
}

eval {
    open my $running_totals_file, '>', RUNNING_TOTALS_FILE or die "that didn't work: $!";
    nstore_fd \%byte_data, \*$running_totals_file;
    close $running_totals_file;
} or die "$!";

open my $out, '>', 'coconet_out.dat';

foreach $source (sort { $a cmp $b } keys %byte_data) {
    print $out "$source\n";
    print $out "  $_ $byte_data{$source}{$_}\n" foreach sort { $a cmp $b } keys %{ $byte_data{$source} };
}
close $out;
