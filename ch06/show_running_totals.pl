#!/usr/bin/perl
use Storable qw( retrieve );

my $hashref = retrieve 'running_totals.dat';
foreach $source (sort keys %$hashref) {
    print "$source\n";
    foreach $dest (sort keys $hashref->{$source}) {
        print "\t$dest: $hashref->{$source}{$dest}\n";
    }
}
