#!/usr/bin/perl
use strict;
use feature 'say';

package Animal {
    sub youre_an_animal { say "You're an animal!" }
}
