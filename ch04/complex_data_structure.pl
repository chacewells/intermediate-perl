#!/usr/bin/perl
use 5.018;
require 'mod_array.pl';

# arrays containing a name (string)
# and an array reference
my @skipper = qw(blue_shirt hat jacket preserver sunscreen);
my @skipper_with_name = ('Skipper' => \@skipper);

my @professor = qw(sunscreen water_bottle slide_rule batteries radio);
my @professor_with_name = ('Professor' => \@professor);

my @gilligan = qw(red_shirt hat lucky_socks water_bottle);
my @gilligan_with_name = ('Gilligan' => \@gilligan);

my @all_with_names = (
    \@skipper_with_name,
    \@professor_with_name,
    \@gilligan_with_name,
);

for my $person (@all_with_names) {
    my $name = $$person[0];
    my @items = @{$person->[1]};
    say "$name was originally (@items)";
}

&check_required_items(@$_) for @all_with_names;

for my $person (@all_with_names) {
    my $name = $person->[0];
    my @items = @{$person->[1]};
    say "$name is now (@items)";
}
