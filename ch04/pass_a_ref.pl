#!/usr/bin/perl
use 5.018;

my $msg = "i can pass references around!";
say $msg;

myupper(\$msg);
say $msg;

sub myupper {
    my $stringref = shift;
    $$stringref =~ s/(.*)/\U$1/;
}
