#!/usr/bin/perl
use strict;
use warnings;

my %gilligan_info = (
	name     => 'Gilligan', 
	hat      => 'White', 
	shirt    => 'Red', 
	position => 'First Mate',
    location => 'The Island',
);
my %skipper_info = (
	name     => 'Skipper',
	hat      => 'Black',
	shirt    => 'Blue',
	position => 'Captain',
    location => 'The Island',
);
my %mr_howell_info = (
    name     => 'Mr. Howell',
    hat      => 'Yellow',
    shirt    => 'Yellow',
    position => 'Passenger',
    location => 'The Island',
);
my %mrs_howell_info = (
    name     => 'Mrs. Howell',
    hat      => 'Blue',
    shirt    => 'Pink',
    position => 'Passenger',
    location => 'The Island',
);

my @crew = (\%gilligan_info, \%skipper_info, \%mr_howell_info, \%mrs_howell_info);

my $format = "%-15s %-7s %-7s %-15s\n"; 
printf $format, qw(Name Shirt Hat Position); 

foreach my $crewmember (@crew) {
	printf $format, @$crewmember{ qw(name shirt hat position) };
}

foreach my $crewmember (@crew) {
    printf "%s is at %s\n", @$crewmember{ qw(name location) };
}

foreach my $crewmember (@crew) {
    $crewmember->{location} = "The Island Country Club"
        if $crewmember->{name} =~ /Howell/;
    printf "%s is at %s\n", @$crewmember{ qw(name location) };
}
