#!/usr/bin/perl
use List::Util qw( sum );
# use POSIX;

my %byte_data;
my @byte_list;

foreach (<>) {
    my($source, $dest, $bytes) = split;
    $byte_data{$source}{$dest} += $bytes;
}

print "sort by largest send, then largest send to individuals\n";
foreach $source ( sort {
    my $total_a = sum map { $byte_data{$a}{$_} } keys %{ $byte_data{$a} };
    my $total_b = sum map { $byte_data{$b}{$_} } keys %{ $byte_data{$b} };
    $total_b <=> $total_a
} keys %byte_data) {
    foreach $dest (sort { $byte_data{$source}{$b} <=> $byte_data{$source}{$a} } keys %{ $byte_data{$source} }) {
        printf "%s => %s: %d\n", $source, $dest, $byte_data{$source}{$dest}
    }
}

print "\nsort by largest individual send.. period\n";
foreach $source (keys %byte_data) {
    foreach $dest (keys %{ $byte_data{$source} }) {
        push @byte_list, {
            source => $source, 
            dest => $dest,
            bytes => $byte_data{$source}{$dest},
        };
    }
}

printf "%s -> %s: %d\n", $_->{source}, $_->{dest}, $_->{bytes}
    foreach sort { $b->{bytes} <=> $a->{bytes} } @byte_list;

open my $out, '>', 'coconet_out.dat';

foreach $source (sort { $a cmp $b } keys %byte_data) {
    print $out "$source\n";
    print $out "  $_ $byte_data{$source}{$_}\n" foreach sort { $a cmp $b } keys %{ $byte_data{$source} };
}
close $out;
