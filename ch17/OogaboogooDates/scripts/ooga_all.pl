use v5.18;
use OogaboogooDates qw( :all );

say '=' x 12, ' number_to_day_name ', '=' x 12;
say "package: ", OogaboogooDates->number_to_day_name($_) for 1..7;
say "no package: ", number_to_day_name($_) for 1..7;
say "no arg: ", number_to_day_name for 1..7;

say '=' x 12, ' number_to_month_name ', '=' x 12;
say "package: ", OogaboogooDates->number_to_month_name($_) for 1..12;
say "no package: ", number_to_month_name($_) for 1..12;
say "no arg: ", number_to_month_name for 1..12;
