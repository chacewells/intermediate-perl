package OogaboogooDates {

use v5.18;
use Carp qw(croak);
use Exporter qw(import);

our @EXPORT_OK = qw( number_to_day_name number_to_month_name );
our %EXPORT_TAGS = (
    all => [ qw( number_to_day_name number_to_month_name ) ],
);

my @days = qw(ark dip wap sen pop sep kir);
my @months = qw(diz pod bod rod sip wax lin sen kun fiz nap dep);

sub number_to_day_name {
    ref(my $num = shift // $_)
        and croak "Cannot invoke class method on an instance variable";
    $num = shift if $num eq __PACKAGE__;

    my $day_name = $days[$num-1];
    croak "day of week must be a number between 1 and 7"
        unless defined $day_name;
    $day_name;
}

sub number_to_month_name {
    ref(my $num = shift // $_)
        and croak "Cannot invoke class method on an instance variable";;
    $num = shift if $num eq __PACKAGE__;

    my $month_name = $months[$num-1];
    croak "month of year must be a number between 1 and 12"
        unless defined $month_name;
    $month_name;
}
}
