use My::List::Utils;
use Test::Simple tests => 4;
no warnings 'all';

# test 1: 
my @nums = (2,5,8,12);
ok( My::List::Utils::sum(@nums) eq 27, 'sum of 2, 5, 8, and 12 is 27' );

# test 2:
my @words = qw( this is the last waterfall );
ok( My::List::Utils::sum(@words) eq 0, "words add like Perl strings, so 0" );

# test 3:
@nums = (1..20);
ok( My::List::Utils::shuffle(@nums) !~ @nums, "shuffled nums is not nums" );

# test 4:
@nums = (1);
ok( My::List::Utils::shuffle(@nums) ~~ @nums, "one number shuffled is the same list" );
