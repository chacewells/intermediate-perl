package My::List::Utils {
use v5.18.2;
use strict;
use warnings;
use Exporter qw(import);
use Carp qw(croak);

our $VERSION = '0.01';
our @EXPORT_OK = qw( sum shuffle );

    sub sum {
        ref (my $num = shift // $_)
            and croak "cannot invoke a class method on an instance variable";
        croak "sum function called with uninitialized value"
            unless defined $num;
        $num = shift if $num eq __PACKAGE__;
        if (@_ == 1) {
            $num
        } else {
            $num + &sum(@_)
        }
    }
    
    sub shuffle {
        croak "nothing to shuffle" unless @_;
        croak "can't call class function on instance" if ref $_[0];
        shift if $_[0] eq __PACKAGE__;
        my @out;
        while (@_) {
            my $idx = int(rand($#_ + 1));
            push(@out, splice(@_, $idx, 1));
        }
        @out
    }
}
