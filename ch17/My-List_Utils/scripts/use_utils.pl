use v5.18;
use My::List::Utils qw(sum shuffle);

my @nums = (1,5,10,20);
say sum @nums;
my $no_panic = My::List::Utils->sum(1, 5, 10, 20);
say "sum of 1,5,10,20: ", $no_panic;
eval { say sum } or warn $@;

say "shuffled: ", join ' ', shuffle @nums;
say "shuffled: ", join ' ', My::List::Utils->shuffle(@nums);
say "shuffled: ", join ' ', My::List::Utils::shuffle(@nums);
