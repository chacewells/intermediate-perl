#!/usr/bin/perl
use 5.014;

use Data::Dump::Streamer;

my @luxuries = qw(Diamonds Furs Caviar);

my $hash = {
  Gilligan     => sub { say 'Howdy Skipper!'     },
  Skipper      => sub { say 'Gilligan!!!!'       },
  'Mr. Howell' => sub { say 'Money money money!' },
  Ginger       => sub { say $luxuries[rand @luxuries] },
  };

Dump $hash;
