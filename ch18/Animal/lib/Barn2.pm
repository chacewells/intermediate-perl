use v5.18;
package Barn2 {
    sub new { bless [ ], shift }
    sub add { push @{shift()}, shift }
    sub contents { @{shift()} }
    sub DESTROY {
        my $self = shift;
        print "$self is being destroyed...\n";
        while (@$self) {
            my $homeless = shift @$self;
            say '  ', $homeless->name, ' goes homeless.';
        }
    }
}
1
