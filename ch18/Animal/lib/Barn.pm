use v5.18;
package Barn {
    sub new { bless [ ], shift }
    sub add { push @{shift()}, shift }
    sub contents { @{shift()} }
    sub DESTROY {
        my $self = shift;
        print "$self is being destroyed...\n";
        print '  ', $_->name, " goes homeless.\n" for $self->contents;
    }
}
1
