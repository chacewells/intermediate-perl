use v5.18;
use Barn2;
use Cow;

my $barn = Barn2->new;
$barn->add(Cow->named($_)) for qw( Bessie Gwen );

say 'Burn the barn:';
$barn = undef;
say 'End of program.';
