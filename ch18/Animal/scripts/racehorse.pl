use v5.18;
use RaceHorse;
my $racer = RaceHorse->named('Billy Bob');

$racer->won;
$racer->won;
$racer->won;
$racer->showed;
$racer->lost;
print $racer->name, " has standings of: ", $racer->standings, ".\n";
