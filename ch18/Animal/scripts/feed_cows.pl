use v5.18;
use Cow;

sub feed_a_cow_named {
    my $name = shift;
    my $cow = Cow->named($name);
    $cow->eat('grass');
    print "Returning from the subroutine.\n";
}

say "Start of program.";
my $outer_cow = Cow->named('Bessie');
say "Now have a cow named $outer_cow->name.";
feed_a_cow_named('Gwen');
say "Returned from subroutine.";
