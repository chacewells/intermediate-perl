#!/usr/bin/perl
use Horse;
use Sheep;
use feature 'say';

my $tv_horse = Horse->named('Mr. Ed');

say "\$tv_horse's sound is ", $tv_horse->sound;
say "\$tv_horse's sound by direct call: ", Horse::sound($tv_horse);
$tv_horse->speak;
say Horse->sound;
say Horse->name;

$tv_horse->eat('hay');
Animal::eat($tv_horse, 'hay');
Sheep->eat('grass');

my $lost = Sheep->named('Bo');
$lost->eat('grass');

# if we implement to return previous
# {
# my $original = $tv_horse->set_color('orange');
# say $tv_horse->name, " is now ", $tv_horse->color;
# $tv_horse->set_color($original);
# say $tv_horse->name, " is now back to ", $tv_horse->color;
# }

# if we implement to return $self for method-chaining
{
    my $beaut =
      Horse->named('Black Beauty')
           ->set_color('black')
           ->set_age(4)
           ->set_height('18 hands');
    
    say $beaut->name, " is ",
        $beaut->color, ", ",
        $beaut->age, " years old, and ",
        $beaut->height, " tall.";
}
