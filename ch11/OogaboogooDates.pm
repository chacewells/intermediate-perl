use v5.12;

package OogaboogooDates {
    my @days = qw(ark dip wap sen pop sep kir);
    my @months = qw(diz pod bod rod sip wax lin sen kun fiz nap dep);

    sub number_to_day_name { my $num = shift;
        my $day_name = $days[$num-1];
        die "day of week must be a number between 1 and 7"
            unless defined $day_name;
        $day_name;
    }

    sub number_to_month_name { my $num = shift;
        my $month_name = $months[$num-1];
        die "month of year must be a number between 1 and 12"
            unless defined $month_name;
        $month_name;
    }
}
