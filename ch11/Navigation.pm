package Navigation;

sub current_heading { 45 }

sub turn_toward_heading {
  my $new_heading = shift;
  my $current_heading = current_heading(  );
  print "Current heading is ", $current_heading, ".\n";
  my $direction = 'right';
  my $turn = ($new_heading - $current_heading) % 360;
  unless ($turn) {
    print "On course (good job!).\n";
    return;
  }
  print "Come about to $new_heading ";
  if ($turn > 180) { # long way around
    $turn = 360 - $turn;
    $direction = 'left';
  }
  print "by turning $direction $turn degrees.\n";
}

sub turn_toward_port {
    # turning toward port
}
1;
