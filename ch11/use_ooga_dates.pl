require 'OogaboogooDates.pm';
use v5.18;

for my $n (1..7) {
    state $day_names = [ qw( blank Sunday Monday Tuesday Wednesday Thursday Friday Saturday ) ];
    state $ooga_day = \&OogaboogooDates::number_to_day_name;
    say "$$day_names[$n] is ", $ooga_day->($n), " for the Oogaboogoo";
}

for my $n (1..12) {
    state $month_names = [ qw( blank January February March April May June July August September October November December ) ];
    state $ooga_mon = \&OogaboogooDates::number_to_month_name;
    say "$$month_names[$n] is ", $ooga_mon->($n), " for the Oogaboogoo";
}
