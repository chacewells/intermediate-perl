#!/usr/bin/perl
require 'OogaboogooDates.pm';

my($year, $mon, $wday, $mday) = do {
    my @ymd = (localtime)[5,4,6,3];
    @ymd = ($ymd[0]+1900, $ymd[1]+1, $ymd[2]+1, $ymd[3]);
    @ymd;
};

($mon, $wday) = (OogaboogooDates::number_to_month_name($mon), OogaboogooDates::number_to_day_name($wday));
print "Today is $wday $mon $mday $year\n";
