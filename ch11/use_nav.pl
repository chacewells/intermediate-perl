#!/usr/bin/perl
require 'Navigation.pm';
require 'DropAnchor.pm';

# using do 'Navigation.pm' loads all code whether or not it's been loaded already
# using eval is just plain messy
# the "require" calls track what has been imported and what hasn't
