package Animal;
use v5.18;
use Moose::Role;
use namespace::autoclean;

our $VERSION = '0.01';

requires qw( sound default_color );

has 'name' => ( is => 'rw' );
has 'color' => (
    is => 'rw',
    default => sub { shift->default_color }
);

sub speak {
    my $self = shift;
    print $self->name, ' goes ', $self->sound, "\n";
}

1;
