package Cow;
use Moose;
use v5.18;

with 'Animal';

sub default_color { 'spotted' }
sub sound { 'mooooo' }

__PACKAGE__->meta->make_immutable;

1;
