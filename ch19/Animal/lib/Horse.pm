package Horse;
use v5.18;
use Moose;
use namespace::autoclean;

our $VERSION = '0.01';

with 'Animal';

sub default_color { 'brown' }
sub sound { 'neigh' }

__PACKAGE__->meta->make_immutable;

1;
