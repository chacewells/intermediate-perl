use v5.18;
use Horse;
use Cow;
use Sheep;

my $talking = Horse->new( name => "Mr. Ed" );
$talking->speak;
my $moo = Cow->new( name => "Bessie" );
$moo->speak;
my $baab = Sheep->new( color => 'white', name => 'Baab' );
$baab->speak;
