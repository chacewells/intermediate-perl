use Test::More tests => 2;
use Horse;

my $ed = Horse->named("Mr. Ed");
is( $ed->name, "Mr. Ed", "Mr. Ed is Mr Ed." );
is( $ed->sound, 'neigh', "Mr. Ed says neigh (He's a horse, of course)" );
